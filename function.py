def handler (event, context):
    # The event variable corresponds to the request body.
    # For example: event['message'];

    # The context variable corresponds to env variables set.
    # For example: context['MY_VARIABLE'];
    
    # Content can be returned as text or JSON.
    return "Hello World!"