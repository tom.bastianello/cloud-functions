from flask import Flask
from flask import request
import simplejson as json

import function
import os

app = Flask("Serverless Functions")

@app.route('/<path:path>', methods=['POST', 'GET'])
def handler(path):
    if request.method == 'POST':
        return function.handler(json.loads(request.data),os.environ)
    else:
        return function.handler({},os.environ)

app.run(debug = False, host = '0.0.0.0')